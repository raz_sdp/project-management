<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;
     protected $hidden = [
        'password','remember_token',
    ];

    protected $fillable = [
        'email',
		'password',
		'fullname',
		'role',
        'joining_date',
        'date_of_birth',
    ];

    function usertasks(){
    	return $this->hasMany('App\Models\UserTask');
    }
    function comment()
    {
        return $this->hasMany('App\Models\Comment');
    }
}
