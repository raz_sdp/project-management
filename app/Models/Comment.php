<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
		'project_id',
		'user_id',
		'task_id',
		'message',
    ];

    function project()
    {
    	return $this->belongsTo('App\Models\Project');
    }
    function user()
    {
        return $this->belongsTo('App\Models\User');
    }
    function task()
    {
        return $this->belongsTo('App\Models\Task');
    }
}
