<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    //
    	protected $fillable = [
		'project_id',
		'title',
		'deadline',
		'user_id',
    ];
    function project()
    {
    	return $this->belongsTo('App\Models\Project');
    }
    function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    function usertasks(){
        return $this->hasMany('App\Models\UserTask');
    }
    function comment()
    {
        return $this->hasMany('App\Models\Comment');
    }
}
