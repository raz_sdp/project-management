<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = [
		'user_id',
		'title',
		'detail',
		'deadline',
    ];

    function task()
    {
    	return $this->hasMany('App\Models\Task');
    }
     public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
     function comment()
    {
        return $this->hasMany('App\Models\Comment');
    }
}
