<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserTask extends Model
{
	 protected $fillable = [
        'user_id',
		'status',
		'task_id',
		'supervisor',
		'assign_date',
		'submission_date',
    ];

    function user(){
    	return $this->belongsTo('App\Models\User');
    }

    function task(){
    	return $this->belongsTo('App\Models\Task');
    }
}
