<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Seeder;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Project;
use App\Models\Comment;
use App\Models\Task;
use Illuminate\Support\Facades\DB;
use Hash;
use Validator;

class AdminController extends Controller
{


	public function admin_dashboard(){
		$users = User::get()->count();
		$projects = Project::get()->count();
		$tasks = Task::get()->count();
		$comments= Comment::get()->count();
		
		$data  = [
			'users' => $users, 'projects' => $projects,'tasks' => $tasks,
			'comments' =>$comments
		];
		

		return view('admin.dashboard', $data);
		return view('admin.list');
	}

	public function userupdate()
	{
		DB::table('users')->where(['id'=> 2])->update(['fullname' => 'tan']);
	}

	public function projectdelete()
	{
		DB::table('users')->where(['id'=> 1])->delete();
	}
	public function projectupdate()
	{
		DB::table('users')->where(['id'=> 2])->update(['fullname' => 'tan']);
	}

	public function create()
	{ 
               // User::create(['email','fullname','fullname','develpoer']);
		DB::table('users')->insert([
			['email' => 'taylor@example.com', 'password' => 'Itech','fullname'=>'itech','role'=>'developer']
			]);

	} 
	public function delete()
	{
		DB::table('users')->where(['id'=> 1])->delete();
	}
	public function update()
	{
		DB::table('users')->where(['id'=> 2])->update(['fullname' => 'tan']);
	}

	function loginprocess(Request $request){
    	$rules = [
    		'email' => 'required|email',
    		'password' => 'required',
    	];

    	$messages = [
    		'email.required' => 'Email field can not be empty',
    		'email.email' => 'Not a valid email',
    		'password.required' => 'Password field can not be empty',
    	];

    	$validator = Validator::make($request->all(),$rules,$messages);

    	if($validator->fails()){
    		return redirect()->back()->withErrors($validator)->withInput();
    	}
    	else{
    		if(Auth::attempt(['email'=>$request->email,'password'=>$request->password])){
    			$role = $users = DB::table('users')->select('role')->where('email', $request->email)->first();
    			//dd($role);
                if($role->role == 'admin'){
                    return redirect()->route('admin')->with(['alert'=>'login successful']);
                }
    		}
    		else{
    			return redirect()->back()->with(['alert'=>'Email or password does not match']);
    		}

    	}
    }
    public function logout(){
        Auth::logout();
        return redirect()->route('loginprocess');
    }


    public function projectlist(){
    	return view('admin.projectlist');
    }
    public function projectcreate(Request $request)
	{ 
               // User::create(['email','fullname','fullname','develpoer']);
		$insert = $request->except('_token');
		$inserted = Project::create($insert);

		if($inserted)
		{
			return redirect()->back()->with(['success'=>'Data Inserted.']);	
			
		}
		return redirect()->back()->with(['fail'=>'Fail to insert data.']);	
	} 

	public function projectcreateview(){
		return view('admin.create_project');
	}

    public function taskcreateview(){
    	return view('admin.create_task');
    }

    public function taskcreate(Request $request)
	{ 
               // User::create(['email','fullname','fullname','develpoer']);
		$insert = $request->except('_token');
		$inserted = Task::create($insert);
		if($inserted)
		{
			return redirect()->back()->with(['success'=>'Data Inserted.']);	
			
		}
		return redirect()->back()->with(['fail'=>'Fail to insert data.']);	
	} 

	 public function usercreateview(){
    	return view('admin.new_user');
    }

    public function usercreateprocess(Request $request)
	{ 
               // User::create(['email','fullname','fullname','develpoer']);
		$insert = $request->except('_token');
		$insert['password'] = Hash::make($request->password);
		$inserted = User::create($insert);
		if($inserted)
		{
			return redirect()->back()->with(['success'=>'Data Inserted.']);	
			
		}
		return redirect()->back()->with(['fail'=>'Fail to insert data.']);	
	} 
}