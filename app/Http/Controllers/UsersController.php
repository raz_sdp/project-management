<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Project;
use App\Models\UserTask;
use App\Models\Task;
use Validator;
use Hash;
class UsersController extends Controller
{
    
    public function index(){
    	$users = User::get();    
        return view('User.index', ['users' => $users]);
    }

 	public function admin_dashboard(){
        return view('admin.list');
    }
	public function user_home(){
        $project = Project::all();
	    return view('frontend.home',['project' => $project]);
	}
	public function user_task($id = null){
        $user_task['usertask'] = UserTask::all();
        $task['task'] = Task::where('project_id', $id)->get();
	    return view('frontend.task',$user_task,$task);
	}
    
    public function edit(){
    	$data['user'] = User::all();
    	return view('admin.edit_delete',$data);
    }

     public function userdelete($id){
    	DB::table('users')->where(['id'=> $id])->delete();
    	$data['user'] = User::all();
    	return view('admin.edit_delete',$data);
    }

    public function usereditview($id){
    	$data = User::select('email','fullname','role','joining_date','date_of_birth')->where('id',$id)->get();
    	//dd($data);
    	return view('admin.update_user',$data);
    }

    public function useredit($id,Request $request){
    	$update = $request->except('_token')->where('id', $id);
		$updated = User::update($update);
    	return view('admin.edit_delete',$data);
    }

}
