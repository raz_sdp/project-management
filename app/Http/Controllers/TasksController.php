<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Task;
class TasksController extends Controller
{
	
	   	function alltask()
	   	{
			$data['task'] = Task::where(['project_id'=>'1'])->get();
			
				return view('admin.tasklist', $data);
			
		}
    	
    	public function taskedit(){
    		$data['task'] = Task::all();
            //dd($data);
    		return view('admin.task_edit_delete',$data);
    	}

     	public function taskdelete($id){
    		DB::table('tasks')->where(['id'=> $id])->delete();
    		$data['task'] = Task::all();
    		return view('admin.task_edit_delete',$data);
    	}
}