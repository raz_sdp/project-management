<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Project;

class ProjectsController extends Controller
{
	function allprojects(){
		$a = Project::all();
	}

	public function projectedit(){
    	$data['project'] = Project::all();
    	return view('admin.project_edit_delete',$data);
    }

     public function projectdelete($id){
    	DB::table('projects')->where(['id'=> $id])->delete();
    	$data['project'] = Project::all();
    	return view('admin.project_edit_delete',$data);
    }
}
