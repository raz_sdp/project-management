<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login',function() {
	return view('User.login');
});

Route::get('/projects',function() {
	return view('Project.project_list');
});

//Route::get('/',['uses'=>'UsersController@index', 'as'=>'/']);
Route::get('/allprojects',['uses'=>'ProjectsController@allprojects', 'as'=>'allprojects']);

Route::get('/allcomments',['uses'=>'CommentsController@allcomments', 'as'=>'allcomments']);

Route::get('/alltask',['uses'=>'TasksController@alltask', 'as'=>'alltask']);
Route::get('/usercreate',['uses'=>'AdminController@usercreate', 'as'=>'usercreate']);
Route::get('/delete',['uses'=>'AdminController@delete', 'as'=>'delete']);
Route::get('/update',['uses'=>'AdminController@update', 'as'=>'update']);

//Route::get('/',['uses'=>'UsersController@index', 'as'=>'home']);
Route::get('/admin',['uses'=>'AdminController@admin_dashboard', 'as'=>'admin']);
Route::get('/projectcreateview',['uses'=>'AdminController@projectcreateview', 'as'=>'projectcreateview']);
Route::post('/projectcreate',['uses'=>'AdminController@projectcreate', 'as'=>'projectcreate']);
Route::post('/loginprocess',['uses'=>'AdminController@loginprocess', 'as'=>'loginprocess']);
Route::get('/projectlist',['uses'=>'AdminController@projectlist', 'as'=>'projectlist']);
Route::get('/taskcreateview',['uses'=>'AdminController@taskcreateview', 'as'=>'taskcreateview']);
Route::post('/taskcreate',['uses'=>'AdminController@taskcreate', 'as'=>'taskcreate']);
Route::get('/usercreateview',['uses'=>'AdminController@usercreateview', 'as'=>'usercreateview']);
Route::post('/usercreateprocess',['uses'=>'AdminController@usercreateprocess', 'as'=>'usercreateprocess']);
Route::get('/edit',['uses'=>'UsersController@edit', 'as'=>'edit']);
Route::get('/userdelete/{id}',['uses'=>'UsersController@userdelete', 'as'=>'userdelete']);
Route::post('/useredit/{id}',['uses'=>'UsersController@edit', 'as'=>'edit']);
Route::get('/usereditview/{id}',['uses'=>'UsersController@usereditview', 'as'=>'usereditview']);
Route::get('/projectedit',['uses'=>'ProjectsController@projectedit', 'as'=>'projectedit']);
Route::get('/projectdelete/{id}',['uses'=>'ProjectsController@projectdelete', 'as'=>'projectdelete']);
Route::get('/taskedit',['uses'=>'TasksController@taskedit', 'as'=>'taskedit']);
Route::get('/taskdelete/{id}',['uses'=>'TasksController@taskdelete', 'as'=>'taskdelete']);


// front end all page 
Route::get('/',['uses'=>'UsersController@user_home', 'as'=>'/']);
Route::get('/user_task/{id}',['uses'=>'UsersController@user_task', 'as'=>'user_task']);