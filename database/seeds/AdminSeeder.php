<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$admin = new User();
		$admin -> email = 'admin@gmail.com';
		$admin -> password = Hash::make('1234');
		$admin -> fullname = 'Admin';
		$admin -> role = 'admin';
		$admin ->save();
    }
}
