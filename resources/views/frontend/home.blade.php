@extends('layout.fontend.master')
@section('front_mainbody')
<!-- BEGIN CONTAINER -->
<div class="page-content page-content-popup">
    <div class="container">
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="todo-main-header" style="margin-top:0px !important">
            <h3>PROJECT WITH MEMBERS </h3>
            <ul class="todo-breadcrumb">
                <li>
                    <a class="todo-active" href="javascrip:;">Todo</a>
                </li>
            </ul>
        </div>
        @if(isset($project) && !empty($project))
        @foreach($project as $data)
        <div class="todo-container">
            <div class="row">
                <div class="col-md-4">
                    <ul class="todo-projects-container">
                        <div class="todo-projects-divider"></div>
                        <li><a class="todo-projects-item" href="{{route('user_task', ['id' =>$data->id])}}">
                                <h3 class="todo-blue">{{$data->title}}</h3>

                                <p>{{$data->detail}}</p>
                                <ul id="user-img">
                                    <?php
                                    if (!empty($data->user)) :
                                        ?>
                                        <li class="mt-comment-img">
                                            <img src="assets/img/users/avatar1.jpg"
                                                 title="<?php echo $data->user->fullname ?>">
                                        </li>
                                    <?php
                                    endif;
                                    ?>

                                </ul>
                            </a>
                        </li>
                    </ul>
                </div>
                @endforeach
                @endif
                <!-- END PAGE BASE CONTENT -->
            </div>
        </div>
    </div>
</div>
<!-- BEGIN CONTAINER -->
@endsection
