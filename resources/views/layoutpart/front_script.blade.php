@yield('before-script')
	{{Html::script('assets/js/app.min.js')}}
	{{Html::script('assets/js/bootstrap.min.js')}}
	{{Html::script('assets/js/bootstrap-switch.min.js')}}
	{{Html::script('assets/js/dashboard.min.js')}}
	{{Html::script('assets/js/daterangepicker.min.js')}}
	{{Html::script('assets/js/jquery.blockui.min.js')}}
	{{Html::script('assets/js/jquery.counterup.min.js')}}
	{{Html::script('assets/js/jquery.min.js')}}
	{{Html::script('assets/js/jquery.slimscroll.min.js')}}
	{{Html::script('assets/js/jquery.waypoints.min.js')}}
	{{Html::script('assets/js/js.cookie.min.js')}}
	{{Html::script('assets/js/layout.min.js')}}
	{{Html::script('assets/js/moment.min.js')}}
	{{Html::script('assets/js/morris.min.js')}}
	{{Html::script('assets/js/quick-nav.min.js')}}
	{{Html::script('assets/js/quick-sidebar.min.js')}}
	{{Html::script('assets/js/raphael-min.js')}}
	{{Html::script('assets/js/timeline.min.js')}}
	{{Html::script('assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js')}}
	{{Html::script('assets/vendor/bootstrap-switch/js/bootstrap-switch.min.js')}}
	{{Html::script('assets/vendor/bootstrap-daterangepicker/daterangepicker.min.js')}}
	{{Html::script('assets/vendor/morris/morris.min.js')}}
	{{Html::script('assets/vendor/morris/raphael-min.js')}}
	{{Html::script('assets/vendor/counterup/jquery.waypoints.min.js')}}
	{{Html::script('assets/vendor/fullcalendar/fullcalendar.min.js')}}
	{{Html::script('assets/vendor/counterup/jquery.counterup.min.js')}}
	{{Html::script('assets/vendor/jqvmap/jqvmap/maps/jquery.vmap.russia.js')}}
	{{Html::script('assets/vendor/jqvmap/jqvmap/maps/jquery.vmap.world.js')}}
	{{Html::script('assets/vendor/jqvmap/jqvmap/maps/jquery.vmap.europe.js')}}
	{{Html::script('assets/vendor/jqvmap/jqvmap/maps/jquery.vmap.germany.js')}}
	{{Html::script('assets/vendor/jqvmap/jqvmap/maps/jquery.vmap.usa.js')}}
	{{Html::script('assets/vendor/jqvmap/jqvmap/data/jquery.vmap.sampledata.js')}}
	{{Html::script('assets/vendor/bootstrap/js/bootstrap.min.js')}}
	{{Html::script('assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js')}}
	{{Html::script('assets/vendor/bootstrap-switch/js/bootstrap-switch.min.js')}}
	{{Html::script('assets/vendor/bootstrap/js/bootstrap.min.js')}}
	{{Html::script('assets/js/custom.js')}}
@yield('after-script')