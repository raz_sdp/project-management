<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>@yield('title'.'Home')</title>
		@include('layoutpart.front_style')
	</head>
	<body class="main-bodys">
    <div class>
		<!-- BEGIN HEADER -->
        <header class="header">
            <nav class="navbar navbar-dark bg-primary">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand my-class" href="#">Project Management</a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">                  
                  <ul class="nav navbar-nav navigation">
                    <li><a href="{{route('/')}}" style="line-height: 35px;">Home</a></li>
<!--                    <li><a href="table_datatables_managed.html" style="line-height: 35px;">Task</a></li>-->
                  </ul>
                </div><!-- /.navbar-collapse -->
            </nav>
        </header>
        <!-- END HEADER -->